
<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("db_connection.php");

if(!class_exists("Cart")){
    class Cart{
        public function insert_cart($article_id,$article_title,$article_price){

            global $db;
            
            $query = "
                INSERT INTO cart(cart_article_id,cart_article_title,cart_article_price) VALUES ('$article_id','$article_title','$article_price')
            ";

            return $db->insert($query);
        }

        public function clear(){
            
            global $db;
           
            $query = "
                TRUNCATE TABLE cart
            ";
            
            return $db->remove($query);
        }

        public function select_cart(){

            global $db;

            $query = "
                SELECT DISTINCT * FROM cart LIMIT 5
                ";

            return $db->select($query);
        }

    }
}
?>