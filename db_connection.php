<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

if(!class_exists('DB')){
    class DB{
        public function __construct(){
            $db = new mysqli("localhost","name","password","database");

            if($db->connect_errno){
                printf("Connection error: %s\n", $db->connect_error);
            }

            $this->connection = $db;
        }

        public function insert($query){

            $db = $this->connection;

            $result = $db->query($query);

            return $result;
        }

        public function update($query){
            $db = $this->connection;

            $result = $db->query($query);

            return $result;
        }

        public function remove($query){
            $db = $this->connection;

            $result = $db->query($query);

            return $result;
        }

        public function select($query){

            $db = $this->connection;

            $result = $db->query($query);

            while($object = $result->fetch_object()){
                $results[] = $object;
            }

            return $results;
        }

        public function row_count($query){

            $db = $this->connection;

            $result = $db->query($query);

            $count = mysqli_num_rows($result);

            return $count;
        }
    }
}

$db = new DB;

?>