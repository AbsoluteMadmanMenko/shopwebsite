
<?php

	session_start();

	include_once("functions.php");
	include_once("query.php");
	include_once("cart.php");


	$cart = new Cart;

	$maxPages = $query->get_row_count();
	if($maxPages < 10){
		$maxPages = 1;
	}else{
		$maxPages = ceil($maxPages / 9);
	}

	if (!isset($_GET['page'])) {
		$page = 1;
	 } else {
		$page = (int)$_GET['page'];
	 }

	 $article_array = $query->all_articles($page);

	 $cat1_array = $query->cat_articles($page,"Poem");
	 $cat2_array = $query->cat_articles($page,"Treatise");
	 $cat3_array = $query->cat_articles($page,"Historical");
	 $cat4_array = $query->cat_articles($page,"Saga");
	 $cat5_array = $query->cat_articles($page,"Revelations");
	 $cat6_array = $query->cat_articles($page,"Biography");

	if(isset($_GET["search"])){
		$search = $_GET["search"];
		$search_array = $query->search($page,$search);
	}

	 $_SESSION['cartMax'] = ((isset($_SESSION['cartMax'])) ? $_SESSION['cartMax'] : 0);
    
	$select = "";

	$price = 0;


?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="style/style.css">
<title>Shop</title>
</head>
<body>	

<header>

<img src="shop.png" alt="logo" class="logo">

<div class="login_bar">
	<nav>
		<ul>
		<?php

						
			if(isset($_SESSION['username']) && is_admin()){
				echo '<li>'.'User: '. $_SESSION['username'].'</li>';
				echo '<li><a href="admin.php">Admin</a></li>';
				echo '<li><a href="logout.php">Logout</a></li>';
			}else if(!isset($_SESSION['username'])){
				echo '<li><a href="login.php">Login</a></li>';
				echo '<li><a href="signup.php">signup</a></li>';
    		}
    		else
    		{
				echo '<li>'.'User: '. $_SESSION['username'].'</li>';
				echo '<li><a href="logout.php">Logout</a></li>';   
			}

			?>
		</ul>
	</nav>
</div>
</header>

<div class="nav_bar">
	<form id="dd" method="post">
	<select id="dropdown" name="dd_list">
		<option value="all">- - - Select category - - -</option>
		<option value="cat1">Poem</option>
		<option value="cat2">Treatise</option>
		<option value="cat3">Historical</option>
		<option value="cat4">Saga</option>
		<option value="cat5">Revelations</option>
		<option value="cat6">Biography</option>
	</select>
	<input type="submit" name="Show_cat" value="Show">
	</form>
	<a href="about.php">
		<div class="nav_item">About Us</div>
	</a>
	<a href="support.php">
		<div class="nav_item">Support</div>
	</a>
	<form class="nav_search" action="index.php" method="GET">
		<input type="text" name="search" placeholder="Search..." required>
		<input type="submit" value="Search">
	</form>
</div>

<div class="wrapper">
	<div class="nested_articles">
		<?php $counter = 0; ?>
			<?php
			if(!isset($_POST['dd_list']) && !isset($_GET["search"])) {
				foreach($article_array as $a): 
					$counter += 1; ?> 
					<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
					<form method="GET">
					<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
					</form>
					<?php if(!empty($_GET[$a->article_id])){
						if(isset($_SESSION['username'])){
							if($_SESSION['cartMax'] < 5){
		
								$_SESSION['cartMax']++;
								echo "<p>added to cart. </p>";
		
								global $cart;
		
								$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
									
								}else{
								$_SESSION['message'] = "Cart limit reached.";
								}
							}else{
							$_SESSION['message'] = "You must be logged in.";
							}
		
							}
						
							?>
						</div>
					<?php endforeach;
		
					$res = 9 - $counter;
					while($res > 0): ?>
						<div><a href=""><p> No article </p></a></div>
					<?php 
					$res--;
				endwhile;	
				$_GET['dd_list'] = "";
			}
			?>
			<?php
			if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "all"){
					foreach($article_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
			}
			?>
			<?php
			if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat1"){
					foreach($cat1_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
		  	}
		 	if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat2"){
					foreach($cat2_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
	 		}
	  		if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat3"){
					foreach($cat3_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
			}
			if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat4"){
					foreach($cat4_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
			 }

			 if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat5"){
					foreach($cat5_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
			 }

			 if(isset($_POST['dd_list'])) {
				if(htmlspecialchars($_POST['dd_list']) == "cat6"){
					foreach($cat6_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
							if(isset($_SESSION['username'])){
								if($_SESSION['cartMax'] < 5){
			
									$_SESSION['cartMax']++;
									echo "<p>added to cart. </p>";
			
									global $cart;
			
									$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
										
									}else{
									$_SESSION['message'] = "Cart limit reached.";
									}
								}else{
								$_SESSION['message'] = "You must be logged in.";
								}
			
								}
							
								?>
							</div>
						<?php endforeach;
			
						$res = 9 - $counter;
						while($res > 0): ?>
							<div><a href=""><p> No article </p></a></div>
						<?php 
						$res--;
					endwhile;
					$_GET['dd_list'] = "";
				}
			}
			 
			 if(isset($_GET['search'])) {
					foreach($search_array as $a): 
						$counter += 1; ?> 
						<div><a href="article.php?id=<?php echo $a->article_id ?>"><h4><?php echo $a->article_title?></h4><h5><?php echo $a->article_price?> eur</h5></a>
						<form method="GET">
						<input action="index.php" type="submit" name="<?php echo $a->article_id ?>" value="add to cart">
						</form>
						<?php if(!empty($_GET[$a->article_id])){
						if(isset($_SESSION['username'])){
							if($_SESSION['cartMax'] < 5){
			
								$_SESSION['cartMax']++;
								echo "<p>added to cart. </p>";
		
								global $cart;
		
								$cart->insert_cart($a->article_id,$a->article_title,$a->article_price);
									
								}else{
								$_SESSION['message'] = "Cart limit reached.";
								}
							}else{
							$_SESSION['message'] = "You must be logged in.";
							}
			
							}
						
							?>
						</div>
					<?php endforeach;
			
					$res = 9 - $counter;
					while($res > 0): ?>
						<div><a href=""><p> No article </p></a></div>
					<?php 
					$res--;
				endwhile;
				$_GET["search"] = "";
			}
	 		

			?>
			
	</div>

	<div class="menu_right">
		<h3> Cart </h3>
			
				<?php foreach($article_array as $a): ?>
					<?php if(!empty($_GET[$a->article_id]) && isset($_SESSION['username'])){
					global $cart;
					
					$cart_r = $cart->select_cart();
						
						foreach($cart_r as $r){
							
							?><div class=item><?php

								echo "<p>" .$r->cart_article_title ."</p>". "<p> Article price: " .$r->cart_article_price ."</p>";
								$price += $r->cart_article_price;

							?></div><?php

						}
						?>
					<form id="reset" method="post">
						<input type="submit" name="cart_reset" value="Reset">
						<input type="submit" name="cart_order" value="Purchase">
					</form>

					<div class="price">
						<h5> Total Price: <?php echo $price ?> </h5>
					</div>
						<?php
					}

					
				?>	
				<?php endforeach ?>
			
		</div>
</div>

<?php

if(isset($_POST['cart_reset'])){

	global $cart;
	$cart->clear();
	$price = 0;
	header("location: index.php");
	$_SESSION['cartMax'] = 0;
}

if(isset($_POST['cart_order'])){

	
	global $cart;
	$cart->clear();
	$price = 0;
	$_SESSION['cartMax'] = 0;
	$_SESSION['message_success'] = "Thank you for your order.";
	header("location: index.php");
	exit;
	
}

if(!isset($_SESSION['username'])){

	global $cart;
	$cart->clear();
}

?>

<?php if ($page > 1): ?>
<div class="button">
   <a href="index.php?page=<?= $page - 1 ?>">Prev</a>
</div>
<?php else: ?>
<div class="button">
	<a href="">Prev</a>
</div>
<?php endif ?> 
<?php if ($page != $maxPages): ?>
<div class="button">
   <a href="index.php?page=<?= $page + 1 ?>">Next</a>
</div>
<?php else: ?>
<div class="button">
	<a href="">Next</a>
</div>
<?php endif ?>

<?php
    
    if (isset($_SESSION['message'])){
        echo("<div id='error_msg'>" .$_SESSION['message']. "</div>");
        unset($_SESSION['message']);
	}
	
	if(isset($_SESSION['message_success'])){
		echo("<div id='success_msg'>" .$_SESSION['message_success']. "</div>");
		unset($_SESSION['message_success']);
	}

?>

<footer>
	<div class="footer">
		<p> Copyright @ Nemanja Radenkovic 2018 </p>
	</div>
</footer>

</body>
</html>
