<?php

session_start();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$db = mysqli_connect('localhost','name','password','database');

if(mysqli_connect_errno()){
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

if(isset($_POST['btn_login'])){
 
    $username = mysqli_real_escape_string($db,$_POST['username']);
    $password = mysqli_real_escape_string($db,$_POST['password']);

    $password = md5($password);
    $sql = "SELECT * FROM users WHERE username='$username' AND password='$password'";
    $result = mysqli_query($db,$sql);

    if(mysqli_num_rows($result) == 1){
        $_SESSION['message'] = "You are now logged in.";
        $_SESSION['username'] = $username;
        header("location: index.php");
    }else{
        $_SESSION['message'] = "Username/Password incorrect.";
    }
}

mysqli_close($db);

?>


<html>
<head>
    <link rel="stylesheet" type="text/css" href="style/style_login.css">
</head>
<body>
<div class="header">
    <h2>Login</h2>
</div>

<form method="post" action="login.php">
    <table>
        <tr>
            <td>Username:</td>
            <td><input type="text" name="username" class="textInput"></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password" class="textInput"></td>
        </tr>
        <tr>
            <td>Submit</td>
            <td><input type="submit" name="btn_login" value="Login"></td>
        </tr>
    </table>
</form>

<?php
    
    if (isset($_SESSION['message'])){
        echo("<div id='error_msg'>" .$_SESSION['message']. "</div>");
        unset($_SESSION['message']);
    }

?>


</body>
</html>