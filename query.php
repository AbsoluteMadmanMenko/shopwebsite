
<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("db_connection.php");

if(!class_exists("Query")){
    class Query{
        public function all_articles($page){
            global $db;

            $offset = ($page - 1) * 9;

            $query = "
                SELECT * FROM articles LIMIT $offset, 9
            ";

            return $db->select($query);
        }

        public function cat_articles($page,$cat){
            global $db;

            $offset = ($page - 1) * 9;

            $query = "
                SELECT * FROM articles WHERE article_cat='$cat' LIMIT $offset, 9
            ";

            return $db->select($query);
        }

        public function search($page,$search_text){
            global $db;

            $offset = ($page - 1) * 9;

            $query = "
                SELECT * FROM articles WHERE article_title='$search_text' LIMIT $offset, 9
            ";

            return $db->select($query);
        }

        public function article($articleid){

            global $db;

            $query = "
                SELECT * FROM articles WHERE article_id='$articleid'
            ";

            return $db->select($query);
        }

        public function get_row_count(){

            global $db;

            $query = "
                SELECT * FROM articles;
            ";

            return $db->row_count($query);
        }
    }

}

$query = new Query;

?>
