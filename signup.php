<?php
    session_start();

    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);

    $db = mysqli_connect('localhost','name','password','database');

    if(mysqli_connect_errno()){
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    if(isset($_POST['btn_register'])){
        $username = mysqli_real_escape_string($db,$_POST['username']);
        $email = mysqli_real_escape_string($db,$_POST['email']);
        $password = mysqli_real_escape_string($db,$_POST['password1']);
        $password2 = mysqli_real_escape_string($db,$_POST['password2']);

        if($password == $password2){
            $password = md5($password);
            $sql = "INSERT INTO users (username,email,password) VALUES ('$username','$email','$password')";
            $result = mysqli_query($db,$sql);
            if(!$result){
                die("query did not succeed: " . mysqli_error($db));
            }

            $_SESSION['message'] = "Registration successful.";
            $_SESSION['username'] = $username;
            header("location: index.php");
        }else{
            $_SESSION['message'] = "Passwords do not match. Try again.";
        }

    }else{

    }

    mysqli_close($db);

?>


<html>
<head>
    <link rel="stylesheet" type="text/css" href="style/style_signup.css">
</head>
<body>
<div class="header">
    <h2>Registration</h2>
</div>

<form method="post" action="signup.php">
    <table>
        <tr>
            <td>Username:</td>
            <td><input type="text" name="username" class="textInput"></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><input type="text" name="email" class="textInput"></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password1" class="textInput"></td>
        </tr>
        <tr>
            <td>Repeat Password:</td>
            <td><input type="password" name="password2" class="textInput"></td>
        </tr>
        <tr>
            <td>Submit</td>
            <td><input type="submit" name="btn_register" value="Register"></td>
        </tr>
    </table>
</form>

<?php
    
    if (isset($_SESSION['message'])){
        echo("<div id='error_msg'>" .$_SESSION['message']. "</div>");
        unset($_SESSION['message']);
    }

?>

</body>
</html>

